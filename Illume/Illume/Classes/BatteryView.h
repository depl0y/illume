//
//  BatteryView.h
//  Illume
//
//  Created by Wim Haanstra on 16-11-11.
//  Copyright (c) 2011 WimHaanstra.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BatteryView : UIView

@property (nonatomic, retain) UIImage*	batteryImage;
@property (nonatomic, retain) UIImage*	loadImage;
@property float batteryLevel;

@end
