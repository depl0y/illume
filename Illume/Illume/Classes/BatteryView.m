//
//  BatteryView.m
//  Illume
//
//  Created by Wim Haanstra on 16-11-11.
//  Copyright (c) 2011 WimHaanstra.com. All rights reserved.
//

#import "BatteryView.h"

@implementation BatteryView

@synthesize batteryImage, batteryLevel, loadImage;

#define BATTERY_RIGHT_CAP 19
#define BATTERY_LEFT_CAP 11
#define BATTERY_HEIGHT 12
#define BATTERY_MAX_WIDTH 13

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		
		self.backgroundColor = [UIColor clearColor];
		self.batteryImage = [[UIImage imageNamed:@"battery.png"] stretchableImageWithLeftCapWidth:BATTERY_LEFT_CAP topCapHeight:0.0];
		self.loadImage = [UIImage imageNamed:@"battery_background.png"];
		[[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
		self.batteryLevel = [UIDevice currentDevice].batteryLevel;
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(batteryLevelDidChange:)
													 name:UIDeviceBatteryLevelDidChangeNotification object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(batteryStateDidChange:)
													 name:UIDeviceBatteryStateDidChangeNotification object:nil];
    }
	
    return self;
}

- (void)batteryLevelDidChange:(NSNotification *)notification
{
	batteryLevel = [UIDevice currentDevice].batteryLevel;
	[self setNeedsDisplay];
}


- (void)batteryStateDidChange:(NSNotification *)notification
{
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
	[self.batteryImage drawInRect:CGRectMake(0,0,rect.size.width, BATTERY_HEIGHT)];
	
	if (self.batteryLevel != -1)
	{
		float currentLoadWidth = BATTERY_MAX_WIDTH * batteryLevel;
		CGRect loadRect = CGRectMake(3, 3, currentLoadWidth, 6);
		
		[self.loadImage drawInRect:loadRect];
	}
}


@end
