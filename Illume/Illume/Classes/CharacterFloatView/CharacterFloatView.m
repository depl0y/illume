//
//  CharacterFloatView.m
//  Illume
//
//  Created by Wim Haanstra on 17-11-11.
//  Copyright (c) 2011 WimHaanstra.com. All rights reserved.
//

#import "CharacterFloatView.h"
#import "TorchHandler.h"

typedef enum {
	kCharacterPositionLeft,
	kCharacterPositionMiddle,
	kCharacterPositionRight
} kCharacterPosition;

@implementation CharacterFloatView

@synthesize text;
@synthesize characterIndex;
@synthesize labelArray;

#define LEFT_CHAR_SIZE 75
#define CHAR_SIZE 200
#define RIGHT_CHAR_SIZE 75

#define SIDE_CHAR_Y 50
#define MIDDLE_CHAR_Y 0

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		
		self.backgroundColor = [UIColor clearColor];
		self.labelArray = [[NSMutableArray alloc] init];
		
    }
    return self;
}

- (NSString*) getCharacterForPosition:(kCharacterPosition) charPos
{
	if (charPos == kCharacterPositionLeft)
	{
		if (self.characterIndex - 1 >= 0)
		{
			NSRange range = NSMakeRange(self.characterIndex - 1, 1);
			return [self.text substringWithRange:range];
		}
		else
			return nil;
	}
	else if (charPos == kCharacterPositionRight)
	{
		if (self.characterIndex + 1 < [self.text length])
		{
			NSRange range = NSMakeRange(self.characterIndex + 1, 1);
			return [self.text substringWithRange:range];
		}
		else
			return nil;
		
	}
	else
	{
		if (self.characterIndex >= 0)
		{
			NSRange characterRange = NSMakeRange(self.characterIndex, 1);
			return [self.text substringWithRange:characterRange];
		}
		else
			return nil;
	}
}

- (void) setText:(NSString *) _text
{
	[text release];
	text = [_text retain];
	self.characterIndex = -1;
	
	if ([self.labelArray count] > 0)
	{
		for (UILabel* lbl in self.labelArray)
		{
			[lbl removeFromSuperview];
		}
		
		[self.labelArray removeAllObjects];
	}
	
	for (int index = 0; index < [self.text length]; index++)
	{

		NSString* character = [self.text substringWithRange:NSMakeRange(index, 1)];
		UILabel* lbl = [[[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width, MIDDLE_CHAR_Y, 200, 250)] autorelease];
		lbl.font = [UIFont boldSystemFontOfSize:CHAR_SIZE];
		lbl.textAlignment = UITextAlignmentCenter;
		lbl.backgroundColor = [UIColor clearColor];
		lbl.text = character;
		lbl.opaque = NO;
		lbl.textColor = [UIColor whiteColor];
		lbl.alpha = 0.1;

		[self addSubview:lbl];
		
		[self.labelArray addObject:lbl];
	}
}

- (void) moveNextCharacter
{
	float animationTime = 0.2;
	
	UILabel*  leftLabel = nil;
	UILabel*  middleLabel = nil;
	UILabel*  rightLabel = nil;
	UILabel*  outOfScreenLabel = nil;

	if (self.characterIndex >= 0 && self.characterIndex < [self.text length])
	{
		UILabel* lbl = [self.labelArray objectAtIndex:self.characterIndex];
		NSString* up = [lbl.text uppercaseString];
		animationTime = [[TorchHandler sharedHandler] MorseLetterTime:up];
	}
	
	animationTime = 0.8;
	
	// Move out of screen
	if (self.characterIndex - 1 >= 0 && self.characterIndex - 1 < [self.text length])
		outOfScreenLabel = [self.labelArray objectAtIndex:self.characterIndex - 1];
	
	self.characterIndex++;
	
	// Move character from middle to left position
	if (self.characterIndex - 1 >= 0 && self.characterIndex - 1 < [self.text length])
		leftLabel = [self.labelArray objectAtIndex:self.characterIndex - 1];
	
	// Move right character to middle position
	if (self.characterIndex < [self.text length])
		middleLabel = [self.labelArray objectAtIndex:self.characterIndex];
	
	// Move from out of screen to right position
	if (self.characterIndex + 1 < [self.text length])
		rightLabel = [self.labelArray objectAtIndex:self.characterIndex + 1];

	[UIView animateWithDuration:animationTime animations:^(void) {
	
		if (outOfScreenLabel != nil)
		{
			[outOfScreenLabel setFrame:CGRectMake(-1 * 300, MIDDLE_CHAR_Y, 200, 250)];
			outOfScreenLabel.alpha = 0;
		}

		if (leftLabel != nil)
		{
			leftLabel.alpha = 0.1;
			[leftLabel setFrame:CGRectMake(-100, MIDDLE_CHAR_Y, 200, 250)];
		}
		
		if (middleLabel !=nil)
		{
			middleLabel.alpha = 1;
			middleLabel.frame = CGRectMake(60, MIDDLE_CHAR_Y, 200, 250);
		}
		
		if (rightLabel != nil)
		{
			//rightLabel.font = [UIFont boldSystemFontOfSize:LEFT_CHAR_SIZE];
			rightLabel.alpha = 0.1;
			[rightLabel setFrame:CGRectMake(220, MIDDLE_CHAR_Y, 200, 250)];
			
		}
	}];
}

- (void) fadeOut
{
	[UIView animateWithDuration:0.5 animations:^(void) {
		for (UILabel* lbl in self.labelArray)
		{
			lbl.alpha = 0;
		}
	} completion:^(BOOL finished) {
		for (UILabel* lbl in self.labelArray)
		{
			[lbl removeFromSuperview];
		}
	}];
	
}

@end
