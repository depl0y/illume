//
//  CharacterFloatView.h
//  Illume
//
//  Created by Wim Haanstra on 17-11-11.
//  Copyright (c) 2011 WimHaanstra.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CharacterFloatView : UIView

@property (nonatomic, retain) NSMutableArray* labelArray;
@property (nonatomic, copy) NSString* text;
@property int characterIndex;

- (void) moveNextCharacter;
- (void) fadeOut;

@end
