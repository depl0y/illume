//
//  TorchHandler.h
//  torchlight
//
//  Created by Wim Haanstra on 15-11-11.
//  Copyright (c) 2011 WimHaanstra.com. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@protocol TorchHandlerDelegate

@optional
- (void) FlashingCharacter:(NSString*) character;
- (void) MorseMessageDone;
@end

@interface TorchHandler : NSObject
{
	AVCaptureDevice*					device;
	id<TorchHandlerDelegate, NSObject>	delegate;
}

+ (TorchHandler*) sharedHandler;

@property (nonatomic, retain) id<TorchHandlerDelegate, NSObject> delegate;
@property (nonatomic, retain) NSDictionary* morseCodes;
@property BOOL IsCancelled;

@property BOOL IsBusy;
@property BOOL shouldBeOn;

- (BOOL) IsOn;
- (void) TurnOn;
- (void) TurnOff;
- (void) Toggle;
- (void) MorseMessage:(NSString*) message;
- (float) MorseLetterTime:(NSString*) character;
- (float) MorseMessageTime:(NSString*) message;

- (void) CancelMessage;

@end
