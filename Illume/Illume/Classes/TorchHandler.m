//
//  TorchHandler.m
//  torchlight
//
//  Created by Wim Haanstra on 15-11-11.
//  Copyright (c) 2011 WimHaanstra.com. All rights reserved.
//

#import "TorchHandler.h"

#define SHORT_TIME 0.2
#define LONG_TIME 0.6		

@implementation TorchHandler

@synthesize delegate, shouldBeOn, IsBusy;
@synthesize morseCodes, IsCancelled;

static TorchHandler* sharedTorchHandler = nil;

+ (TorchHandler*) sharedHandler
{
	@synchronized(self) {
		if (!sharedTorchHandler)
		{
			sharedTorchHandler = [[TorchHandler alloc] init];
			
			NSString* finalPath = [[NSBundle mainBundle] pathForResource:@"MorseCodes" ofType:@"plist"];
			sharedTorchHandler.morseCodes = [[NSDictionary dictionaryWithContentsOfFile:finalPath] retain];
		}
	}
	
	return sharedTorchHandler;
}

- (void) CancelMessage
{
	self.IsCancelled = YES;
}

- (BOOL) IsOn
{
	if ([AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode == AVCaptureTorchModeOff)
		return false;
	else// if (device.torchMode == AVCaptureTorchModeOn)
		return true;
}

- (void) TurnOn
{
	if ([[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] hasTorch] && 
		[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode == AVCaptureTorchModeOff)
	{
		self.shouldBeOn = YES;
		[[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] lockForConfiguration:nil];
		[[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] setTorchMode:AVCaptureTorchModeOn];
		[[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] unlockForConfiguration];
	}	
}

- (void) TurnOff
{
	if ([[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] hasTorch] && 
		[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode == AVCaptureTorchModeOn)
	{
		self.shouldBeOn = NO;
		[[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] lockForConfiguration:nil];
		[[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] setTorchMode:AVCaptureTorchModeOff];
		[[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] unlockForConfiguration];
	}		
}

- (void) Toggle
{

	if ([[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] hasTorch] && 
		[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode == AVCaptureTorchModeOn)
	{
		[[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] lockForConfiguration:nil];
		[[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] setTorchMode:AVCaptureTorchModeOff];
		[[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] unlockForConfiguration];
		self.shouldBeOn = NO;
	}		
	else if ([[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] hasTorch] && 
			 [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode == AVCaptureTorchModeOff)
	{
		[[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] lockForConfiguration:nil];
		[[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] setTorchMode:AVCaptureTorchModeOn];
		[[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] unlockForConfiguration];
		self.shouldBeOn = YES;
	}		
}

- (NSString*) getLightModes:(NSString*) character
{
	NSString* lookupString = character;

	if ([lookupString isEqualToString:@"."])
		lookupString = @"DOT";
	else if ([lookupString isEqualToString:@","])
		lookupString = @"COMMA";
	else if ([lookupString isEqualToString:@"?"])
		lookupString = @"QUESTION";
	else if ([lookupString isEqualToString:@"'"])
		lookupString = @"SQUOTE";
	else if ([lookupString isEqualToString:@"!"])
		lookupString = @"EXCL";
	else if ([lookupString isEqualToString:@"/"])
		lookupString = @"FSLASH";
	else if ([lookupString isEqualToString:@"("])
		lookupString = @"POPEN";
	else if ([lookupString isEqualToString:@")"])
		lookupString = @"PCLOSE";
	else if ([lookupString isEqualToString:@"&"])
		lookupString = @"AMP";
	else if ([lookupString isEqualToString:@":"])
		lookupString = @"COLON";
	else if ([lookupString isEqualToString:@";"])
		lookupString = @"SEMICOLON";
	else if ([lookupString isEqualToString:@"="])
		lookupString = @"EQUALS";
	else if ([lookupString isEqualToString:@"+"])
		lookupString = @"PLUS";
	else if ([lookupString isEqualToString:@"-"])
		lookupString = @"MINUS";
	else if ([lookupString isEqualToString:@"_"])
		lookupString = @"UNDERSCORE";
	else if ([lookupString isEqualToString:@"\""])
		lookupString = @"QUOTE";
	else if ([lookupString isEqualToString:@"$"])
		lookupString = @"DOLLAR";
	else if ([lookupString isEqualToString:@"@"])
		lookupString = @"AT";
	else if ([lookupString isEqualToString:@" "])
		lookupString = @"SPACE";
		
	return [self.morseCodes objectForKey:lookupString];
}

- (float) MorseLetterTime:(NSString*) character
{
	NSString* lightModes = [self getLightModes:character];
	
	float returnValue = 0.0;
	
	for (int index = 0; index < [lightModes length]; index++)
	{
		if ([lightModes characterAtIndex:index] == 'S')
			returnValue += SHORT_TIME;
		else if ([lightModes characterAtIndex:index] == 'L')
			returnValue += LONG_TIME;
		
		returnValue += SHORT_TIME;
	}
	
	return returnValue;
}

- (float) MorseMessageTime:(NSString*) message
{
	NSString* upperCaseMessage = [message uppercaseString];
	
	float returnValue = 0;
	for (int index = 0; index < [upperCaseMessage length]; index++)
	{
		NSRange range = NSMakeRange(index, 1);
		NSString* letter = [upperCaseMessage substringWithRange:range];
		
		if (![letter isEqualToString:@" "])
		{
			returnValue += [self MorseLetterTime:letter];
			returnValue += LONG_TIME;
		}
		else
			returnValue += 7 * SHORT_TIME;
	}
	
	return returnValue;
}

- (void) MorseLetter:(NSString*) lightModes
{
	for (int index = 0; index < [lightModes length]; index++)
	{
		if ([lightModes characterAtIndex:index] == 'S')
		{
			[self TurnOn];
			[NSThread sleepForTimeInterval:SHORT_TIME];
			[self TurnOff];
		}
		else if ([lightModes characterAtIndex:index] == 'L')
		{
			[self TurnOn];
			[NSThread sleepForTimeInterval:LONG_TIME];
			[self TurnOff];
		}
		[NSThread sleepForTimeInterval:SHORT_TIME];
		
		if (self.IsCancelled)
			break;
	}
}

- (void) MorseMessage:(NSString*) message
{
	if (self.IsBusy)
		return;
	
	self.IsCancelled = NO;
	
	NSString* upperCaseMessage = [message uppercaseString];
	self.IsBusy = YES;
	
	for (int index = 0; index < [upperCaseMessage length]; index++)
	{
		NSRange range = NSMakeRange(index, 1);
		NSString* letter = [upperCaseMessage substringWithRange:range];
		NSString* lightModes = [self getLightModes:letter];
		
		if (![letter isEqualToString:@" "])
		{
			if ([delegate respondsToSelector:@selector(FlashingCharacter:)])
				[delegate FlashingCharacter:[NSString stringWithFormat:@"%@", letter]];
		}
		else
		{
			if ([delegate respondsToSelector:@selector(FlashingCharacter:)])
				[delegate FlashingCharacter:@" "];
		}
		
		if ([lightModes isEqualToString:@"W"])
		{
			[NSThread sleepForTimeInterval:7 * SHORT_TIME];
		}
		else
		{
			[self MorseLetter:lightModes];
			[NSThread sleepForTimeInterval:LONG_TIME];
		}
		
		if (self.IsCancelled)
			break;
	}
	
	if ([delegate respondsToSelector:@selector(MorseMessageDone)])
	{
		self.IsBusy = NO;
		[delegate MorseMessageDone];
	}
}




@end
