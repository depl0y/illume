//
//  ViewController.h
//  Illume
//
//  Created by Wim Haanstra on 16-11-11.
//  Copyright (c) 2011 WimHaanstra.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TorchHandler.h"
#import "BatteryView.h"

@interface ViewController : UIViewController <TorchHandlerDelegate> {

	TorchHandler*	tHandler;
	BatteryView*	batteryView;

}

@property (nonatomic, retain) IBOutlet UIButton*	btnLightSwitch;
@property (nonatomic, retain) IBOutlet UIButton*	btnMorseButton;

- (IBAction)btnLightSwitch_Click:(id)sender;
- (IBAction)btnMorseButton_Click:(id)sender;

@end
