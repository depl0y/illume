//
//  ViewController.m
//  Illume
//
//  Created by Wim Haanstra on 16-11-11.
//  Copyright (c) 2011 WimHaanstra.com. All rights reserved.
//

#import "ViewController.h"
#import "MorseViewController.h"

@implementation ViewController

@synthesize btnLightSwitch, btnMorseButton;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[TorchHandler sharedHandler].delegate = self;
	
	batteryView = [[BatteryView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 26, 4, 22, 12)];
	batteryView.alpha = 0.7;
	[self.view addSubview:batteryView];
	
	NSLogRect(CGRectMake(10,10,100,100));
}

- (IBAction)btnLightSwitch_Click:(id)sender
{
	[[TorchHandler sharedHandler] Toggle];
	if ([TorchHandler sharedHandler].IsOn)
		[btnLightSwitch setImage:[UIImage imageNamed:@"power_button_on.png"] forState:UIControlStateNormal];
	else
		[btnLightSwitch setImage:[UIImage imageNamed:@"power_button.png"] forState:UIControlStateNormal];
	
}

- (IBAction)btnMorseButton_Click:(id)sender
{
	//	[[TorchHandler sharedHandler] MorseMessage:@"WIM"];
	MorseViewController *vc = [[MorseViewController alloc] initWithNibName:@"MorseViewController" bundle:nil];
	vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	[self presentModalViewController:vc animated:YES];
	[vc release];
}

- (void) FlashingCharacter:(NSString *)character
{
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	if (interfaceOrientation == UIInterfaceOrientationPortrait)
		return YES;
	else
		return NO;
}

@end
