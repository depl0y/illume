//
//  AppDelegate.h
//  Illume
//
//  Created by Wim Haanstra on 16-11-11.
//  Copyright (c) 2011 WimHaanstra.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TorchHandler.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;

@end
