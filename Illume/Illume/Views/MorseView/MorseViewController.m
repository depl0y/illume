//
//  MorseViewController.m
//  Illume
//
//  Created by Wim Haanstra on 16-11-11.
//  Copyright (c) 2011 WimHaanstra.com. All rights reserved.
//

#import "MorseViewController.h"

@implementation MorseViewController

#define VIEW_KEYBOARD_MOVE_Y 110

@synthesize btnLightButton, btnStartButton, txtMorseMessage, currentIndex, characterFloatView, timerTickCount, roundProgressBar;
@synthesize batteryView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction)btnLightButton_Click:(id)sender
{
	//	[self.parentViewController dismissModalViewControllerAnimated:YES];
	[self dismissModalViewControllerAnimated:YES];
}

- (IBAction)btnStartButton_Click:(id)sender
{
	if ([TorchHandler sharedHandler].IsBusy)
	{
		[[TorchHandler sharedHandler] CancelMessage];
		return;
	}
	
	if ([txtMorseMessage.text isEqualToString:@""])
		return;
	
	[btnStartButton setImage:[UIImage imageNamed:@"play_button_on.png"] forState:UIControlStateNormal];
	[TorchHandler sharedHandler].delegate = self;
	currentIndex = 0;
	
	[self.characterFloatView setText:txtMorseMessage.text];
	
	float morseTime = [[TorchHandler sharedHandler] MorseMessageTime:txtMorseMessage.text];
	self.roundProgressBar.MaxValue = morseTime;
	
	float timerTickInterval = morseTime / 360;
	
	timerTickCount = 0;
	[NSTimer scheduledTimerWithTimeInterval:timerTickInterval target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
	[NSThread detachNewThreadSelector:@selector(MorseMessage:) toTarget:[TorchHandler sharedHandler] withObject:txtMorseMessage.text];
}

- (void) timerTick:(id) timer
{
	timerTickCount++;

	NSTimer* t = (NSTimer*) timer;
	
	float progress = t.timeInterval * timerTickCount;
	roundProgressBar.Progress = progress;

	if (roundProgressBar.Progress >= roundProgressBar.MaxValue || [TorchHandler sharedHandler].IsCancelled)
	{
		[t invalidate];
	}
}

- (void) FlashingCharacter:(NSString *)character
{
	currentIndex++;
	[NSThread detachNewThreadSelector:@selector(moveNextCharacter) toTarget:self.characterFloatView withObject:nil];
}

- (void) MorseMessageDone
{
	[btnStartButton setImage:[UIImage imageNamed:@"play_button.png"] forState:UIControlStateNormal];
	[self.characterFloatView fadeOut];
//	[self.roundProgressBar setDegrees:0];
	[self.roundProgressBar setProgress:0];
}

- (IBAction) userDoneEnteringText:(id) sender
{
	UITextField* theField = (UITextField*)sender;
	[UIView animateWithDuration:0.3 animations:^(void) {
		self.view.frame = CGRectOffset(self.view.frame, 0, VIEW_KEYBOARD_MOVE_Y);
	}];
	
	[theField resignFirstResponder];	
}

- (IBAction) userStartsEnteringText:(id) sender
{
	[UIView animateWithDuration:0.3 animations:^(void) {
		self.view.frame = CGRectOffset(self.view.frame, 0, VIEW_KEYBOARD_MOVE_Y * -1);
	}];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.characterFloatView = [[CharacterFloatView alloc] initWithFrame:CGRectMake(0, 50, 320, 250)];
	[self.view addSubview:self.characterFloatView];
	
	self.roundProgressBar = [[SBCircleProgressBar alloc] initWithFrame:CGRectMake(116,374, 88,89)];
	[self.view addSubview:self.roundProgressBar];
	
	self.batteryView = [[BatteryView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 26, 4, 22, 12)];
	self.batteryView.alpha = 0.7;
	[self.view addSubview:self.batteryView];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
