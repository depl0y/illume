//
//  MorseViewController.h
//  Illume
//
//  Created by Wim Haanstra on 16-11-11.
//  Copyright (c) 2011 WimHaanstra.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TorchHandler.h"
#import "CharacterFloatView.h"
#import "SBCircleProgressBar.h"
#import "BatteryView.h"

@interface MorseViewController : UIViewController <UITextFieldDelegate, TorchHandlerDelegate>

@property (nonatomic, retain) IBOutlet UIButton*	btnLightButton;
@property (nonatomic, retain) IBOutlet UIButton*	btnStartButton;
@property (nonatomic, retain) IBOutlet UITextField*	txtMorseMessage;

@property (nonatomic, retain) IBOutlet BatteryView* batteryView;

@property (nonatomic, retain) CharacterFloatView*	characterFloatView;
@property (nonatomic, retain) SBCircleProgressBar*		roundProgressBar;

@property int timerTickCount;

@property int currentIndex;

- (IBAction) btnLightButton_Click:(id) sender;
- (IBAction) btnStartButton_Click:(id) sender;

- (IBAction) userDoneEnteringText:(id) sender;
- (IBAction) userStartsEnteringText:(id) sender;

@end
