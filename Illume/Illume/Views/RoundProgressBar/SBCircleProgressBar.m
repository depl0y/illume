//
//  SBCircleProgressBar.m
//
//  Created by Wim Haanstra on 17-11-11.
//  Copyright (c) 2011 Sorted Bits. All rights reserved.
//

#import "SBCircleProgressBar.h"

@implementation SBCircleProgressBar

@synthesize MaxValue, Progress;

// SOME VALUES YOU CAN CHANGE TO CHANGE THE BEHAVIOUR OF THE PROGRESSBAR
#define MAX_DEGREES		360
#define START_DEGREES	270

// Define the radius that the circle needs
#define CIRCLE_RADIUS	27

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		
		self.backgroundColor = [UIColor clearColor];
		self.userInteractionEnabled = NO;

		// Default values for the progress bar
		self.MaxValue = 100.0f;
		self.Progress = 0.0f;
		
    }
    return self;
}

- (void) setMaxValue:(float) _MaxValue
{
	MaxValue = _MaxValue;
	[self setNeedsDisplay];
}

- (void) setProgress:(float) _Progress
{
	Progress = _Progress;
	[self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
	CGContextRef context = UIGraphicsGetCurrentContext(); 
	
	// Set the color of the circle to appear for the progressbar
    CGContextSetRGBStrokeColor(context, 0.341, 0.635, 0.961, 0.6); 
	
	// Set the line width of the circle to appear
    CGContextSetLineWidth(context, 10.0); 
	
	// Calculate the middle of the circle
	CGPoint circleCenter = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
	
	// Calculate the amount of degrees the circle needs to have filled
	float currentDegrees = (MAX_DEGREES / self.MaxValue) * self.Progress;
	
	// Draw the ARC (part of the circle
	CGContextAddArc(context, circleCenter.x , circleCenter.y, CIRCLE_RADIUS, radians(START_DEGREES), radians(START_DEGREES + currentDegrees), 0); 
	CGContextStrokePath(context);
}


@end
