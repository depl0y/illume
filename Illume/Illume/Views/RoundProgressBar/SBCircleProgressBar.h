//
//  SBCircleProgressBar.h
//
//  Created by Wim Haanstra on 17-11-11.
//  Copyright (c) 2011 Sorted Bits. All rights reserved.
//

@interface SBCircleProgressBar : UIView

// The maximum value of the progressbar
@property (nonatomic) float MaxValue;

// The current value of the progressbar
@property (nonatomic) float Progress;

@end
